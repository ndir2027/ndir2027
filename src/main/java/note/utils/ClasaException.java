package note.utils;

public class ClasaException extends Exception {

    public ClasaException(String msg){
        super(msg);
    }
}
