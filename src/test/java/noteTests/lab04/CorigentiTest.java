package lab04;

import note.model.Corigent;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.repository.ClasaRepositoryMock;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class CorigentiTest {

    private ClasaRepositoryMock repositoryMock;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Before
    public void init(){
        repositoryMock = new ClasaRepositoryMock();
    }
    @Test
    public void test1() throws ClasaException {
        Nota nota = new Nota(1, "Desen", 10);
        Nota nota2 = new Nota(1, "Romana",10);
        List<Nota> note = new ArrayList<Nota>();
        note.add(nota);
        note.add(nota2);
        List<Elev> elevi = new ArrayList<Elev>();
        Elev elev = new Elev(1,"Marcel");
        elevi.add(elev);
        repositoryMock.creazaClasa(elevi, note);
        List<Corigent> corigenti = repositoryMock.getCorigenti();
        assertEquals(0, corigenti.size());
    }

    @Test
    public void test2() throws ClasaException {
        Nota nota = new Nota(1, "Desen", 1);
        List<Nota> note = new ArrayList<Nota>();
        note.add(nota);
        List<Elev> elevi = new ArrayList<Elev>();
        Elev elev = new Elev(1,"Marcel");
        elevi.add(elev);
        repositoryMock.creazaClasa(elevi, note);
        List<Corigent> corigenti = repositoryMock.getCorigenti();
        assertEquals(1, corigenti.size());
    }

    @Test
    public void test3() throws ClasaException {
        Nota nota = new Nota(1, "Desen", 2);
        Nota nota2 = new Nota(1, "Romana",1);
        List<Nota> note = new ArrayList<Nota>();
        note.add(nota);
        note.add(nota2);
        List<Elev> elevi = new ArrayList<Elev>();
        Elev elev = new Elev(1,"Marcel");
        elevi.add(elev);
        repositoryMock.creazaClasa(elevi, note);
        List<Corigent> corigenti = repositoryMock.getCorigenti();
        assertEquals(1, corigenti.size());
    }

    @Test
    public void test4() throws ClasaException {
        List<Nota> note = new ArrayList<Nota>();
        List<Elev> elevi = new ArrayList<Elev>();
        repositoryMock.creazaClasa(elevi, note);
        List<Corigent> corigenti = repositoryMock.getCorigenti();
        assertEquals(0, corigenti.size());;
    }

    @Test
    public void test5() throws ClasaException {
        Nota nota = new Nota(1, "Desen", 3);
        Nota nota2 = new Nota(2, "Romana",2);
        Nota nota3 = new Nota(1, "Romana",10);
        List<Nota> note = new ArrayList<Nota>();
        note.add(nota);
        note.add(nota2);
        note.add(nota3);
        List<Elev> elevi = new ArrayList<Elev>();
        Elev elev = new Elev(1,"Marcel");
        Elev elev2 = new Elev(2,"Adina");
        elevi.add(elev);
        elevi.add(elev2);
        repositoryMock.creazaClasa(elevi, note);
        List<Corigent> corigenti = repositoryMock.getCorigenti();
        assertEquals(2, corigenti.size());
    }

    @Test
    public void test6() throws ClasaException {
        Nota nota = new Nota(1, "Desen", 1);
        Nota nota2 = new Nota(1, "Romana",1);
        List<Nota> note = new ArrayList<Nota>();
        note.add(nota);
        note.add(nota2);
        List<Elev> elevi = new ArrayList<Elev>();
        Elev elev = new Elev(1,"Marcel");
        elevi.add(elev);
        repositoryMock.creazaClasa(elevi, note);
        List<Corigent> corigenti = repositoryMock.getCorigenti();
        assertEquals(1, corigenti.size());
        assertEquals(2, corigenti.get(0).getNrMaterii());
    }

    @Test
    public void test7() throws ClasaException {
        Nota nota = new Nota(1, "Desen", 3);
        Nota nota2 = new Nota(2, "Romana",2);
        Nota nota3 = new Nota(1, "Romana",10);
        List<Nota> note = new ArrayList<Nota>();
        note.add(nota);
        note.add(nota2);
        note.add(nota3);
        List<Elev> elevi = new ArrayList<Elev>();
        Elev elev = new Elev(1,"Florin");
        Elev elev2 = new Elev(2,"Adina");
        elevi.add(elev);
        elevi.add(elev2);
        repositoryMock.creazaClasa(elevi, note);
        List<Corigent> corigenti = repositoryMock.getCorigenti();
        assertEquals(2, corigenti.size());
        assertEquals("Adina", corigenti.get(0).getNumeElev());
        assertEquals("Florin", corigenti.get(1).getNumeElev());
    }


}