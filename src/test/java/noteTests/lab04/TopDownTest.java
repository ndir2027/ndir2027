package lab04;

import note.controller.NoteController;
import note.model.Corigent;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.repository.ClasaRepositoryMock;
import note.repository.NoteRepository;
import note.repository.NoteRepositoryMock;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class TopDownTest {

    private ClasaRepositoryMock clasaRepositoryMock;
    private NoteRepository noteRepositoryMock;

    private NoteController ctrl;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Before
    public void init(){
        clasaRepositoryMock = new ClasaRepositoryMock();
        noteRepositoryMock = new NoteRepositoryMock();
        ctrl = new NoteController();
    }


    @Test
    public void modulA() throws ClasaException {
        Nota nota = new Nota(1, "Desen", 3);
        Nota nota2 = new Nota(1, "Romana", 5);
        noteRepositoryMock.addNota(nota);
        noteRepositoryMock.addNota(nota2);
        assertEquals(2, noteRepositoryMock.getNote().size());
    }

    @Test
    public void modulB() throws ClasaException {
        modulA();
        List<Elev> elevi = new ArrayList<Elev>();
        Elev elev = new Elev(1, "Florinel");
        elevi.add(elev);
        clasaRepositoryMock.creazaClasa(elevi, noteRepositoryMock.getNote());
        List<Medie> medii = clasaRepositoryMock.calculeazaMedii();
        assertEquals(1, medii.size());
    }

    @Test
    public void modulC() throws ClasaException {
        modulB();
        List<Medie> medii = clasaRepositoryMock.calculeazaMedii();
        List<Corigent> corigenti = clasaRepositoryMock.getCorigenti();
        assertEquals(1, corigenti.size());
    }


}