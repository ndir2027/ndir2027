package note.repository;

import note.controller.NoteController;
import note.model.Nota;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.*;

public class NoteRepositoryMockTest {

    private NoteRepositoryMock repositoryMock;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Before
    public void init(){
        repositoryMock = new NoteRepositoryMock();
    }
    @Test
    public void test1() throws ClasaException {
        Nota nota = new Nota(1, "Desen", 10);
        repositoryMock.addNota(nota);
        assertEquals(1, repositoryMock.getNote().size());
    }

    @Test
    public void test2() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        Nota nota = new Nota(1, "Istorie", 11);
        repositoryMock.addNota(nota);
    }

    @Test
    public void test3() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        Nota nota = new Nota(1, "Istorie", 0);
        repositoryMock.addNota(nota);
    }

    @Test
    public void test4() throws ClasaException {
        Nota nota = new Nota(1, "Desen", 1);
        repositoryMock.addNota(nota);
        assertEquals(1, repositoryMock.getNote().size());
    }

    @Test
    public void test5() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        Nota nota = new Nota(1, "Istorie", -2);
        repositoryMock.addNota(nota);
    }

    @Test
    public void test6() throws ClasaException {
        Nota nota = new Nota(999, "Istorie", 10);
        repositoryMock.addNota(nota);
        assertEquals(1, repositoryMock.getNote().size());
    }

    @Test
    public void test7() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNrmatricol);
        Nota nota = new Nota(1001, "Istorie", 5);
        repositoryMock.addNota(nota);
    }

    @Test
    public void test8() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNrmatricol);
        Nota nota = new Nota(-1001, "Istorie", 5);
        repositoryMock.addNota(nota);
    }

    @Test
    public void test9() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidMateria);
        Nota nota = new Nota(1, "", 5);
        repositoryMock.addNota(nota);
    }

    @Test
    public void test10() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidMateria);
        Nota nota = new Nota(1, "", 5);
        repositoryMock.addNota(nota);
    }

}