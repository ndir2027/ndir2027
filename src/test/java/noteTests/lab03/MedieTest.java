package lab03;

import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.repository.ClasaRepositoryMock;
import note.repository.NoteRepositoryMock;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class MedieTest {

    private ClasaRepositoryMock repositoryMock;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Before
    public void init(){
        repositoryMock = new ClasaRepositoryMock();
    }
    @Test
    public void test1() throws ClasaException {
        Nota nota = new Nota(1, "Desen", 10);
        Nota nota2 = new Nota(1, "Romana",10);
        List<Nota> note = new ArrayList<Nota>();
        note.add(nota);
        note.add(nota2);
        List<Elev> elevi = new ArrayList<Elev>();
        Elev elev = new Elev(1,"Marcel");
        elevi.add(elev);
        repositoryMock.creazaClasa(elevi, note);
        List<Medie> medii = repositoryMock.calculeazaMedii();
        assertEquals(1, medii.size());
    }

    @Test
    public void test2() throws ClasaException {
        Nota nota = new Nota(1, "Desen", 10);
        Nota nota2 = new Nota(1, "Romana",10);
        List<Nota> note = new ArrayList<Nota>();
        note.add(nota);
        note.add(nota2);
        List<Elev> elevi = new ArrayList<Elev>();
        Elev elev = new Elev(1,"Marcel");
        elevi.add(elev);
        repositoryMock.creazaClasa(elevi, note);
        List<Medie> medii = repositoryMock.calculeazaMedii();
        assertEquals(10.0, medii.get(0).getMedie());
    }

    @Test
    public void test3() throws ClasaException {
        Nota nota = new Nota(1, "Desen", 10);
        Nota nota2 = new Nota(1, "Romana",10);
        Nota nota3 = new Nota(1, "Romana",10);
        List<Nota> note = new ArrayList<Nota>();
        note.add(nota);
        note.add(nota2);
        note.add(nota3);
        List<Elev> elevi = new ArrayList<Elev>();
        Elev elev = new Elev(1,"Marcel");
        Elev elev2 = new Elev(2,"Adina");
        elevi.add(elev);
        elevi.add(elev2);
        repositoryMock.creazaClasa(elevi, note);
        List<Medie> medii = repositoryMock.calculeazaMedii();
        assertEquals(2, medii.size());
    }

    @Test
    public void test4() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.emptyRepository);
        repositoryMock.calculeazaMedii();
    }

    @Test
    public void test5() throws ClasaException {
        Nota nota = new Nota(1, "Desen", 5);
        Nota nota2 = new Nota(1, "Romana",5);
        Nota nota3 = new Nota(1, "Romana",5);
        List<Nota> note = new ArrayList<Nota>();
        note.add(nota);
        note.add(nota2);
        note.add(nota3);
        List<Elev> elevi = new ArrayList<Elev>();
        Elev elev = new Elev(1,"Marcel");
        Elev elev2 = new Elev(2,"Adina");
        elevi.add(elev);
        elevi.add(elev2);
        repositoryMock.creazaClasa(elevi, note);
        List<Medie> medii = repositoryMock.calculeazaMedii();
        assertEquals(5.0, medii.get(0).getMedie());
    }

    @Test
    public void test6() throws ClasaException {
        Nota nota = new Nota(1, "Desen", 10);
        Nota nota2 = new Nota(1, "Romana",10);
        List<Nota> note = new ArrayList<Nota>();
        note.add(nota);
        note.add(nota2);
        List<Elev> elevi = new ArrayList<Elev>();
        Elev elev = new Elev(1,"Marcel");
        elevi.add(elev);
        repositoryMock.creazaClasa(elevi, note);
        List<Medie> medii = repositoryMock.calculeazaMedii();
        assertEquals(1, medii.get(0).getElev().getNrmatricol());
    }


}